#!/usr/bin/env python3
from http.server import HTTPServer, BaseHTTPRequestHandler
import subprocess
import json

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self._handle()

    def do_POST(self):
        self._handle()

    def _handle(self):
        try:
            self.log_message("command: %s", self.path)
            if self.path == '/scriptstartcsv':
                output=subprocess.run(
                    "cd /reporting-dev; /bin/bash /reporting-dev/refresh-and-generate.sh /reporting-dev/env/extended_lit_ref_new_csv.env",
                    shell=True,
                    capture_output=True,
                )
        finally:
            self.send_response(200)
            # self.send_header("content-type", "application/json")
            self.end_headers()
            self.wfile.write("STANDARD OUTPUT AFTER RUNNING THE `refresh-and-generate.sh` SCRIPT:\n\n".encode())
            self.wfile.write(output.stdout)
            if output.stderr:
                self.wfile.write("AN ERROR OCCURED:\n\n".encode())
                self.wfile.write("I`m sorry Dave, I`m afraid a can`t do that ...\nThere seems to be something wrong with your settings ...\nPlease check the extended_lit_ref_new_csv.env file.\n\n".encode())
                self.wfile.write(output.stderr)

if __name__ == "__main__":
    HTTPServer(("localhost", 4242), Handler).serve_forever()
