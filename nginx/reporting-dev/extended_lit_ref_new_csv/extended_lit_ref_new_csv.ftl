    <#compress>

    <#-- Import common macros and functions -->
    <#import "common_module_traversal_utils.ftl" as utils>
    <#import "macros_common_general.ftl" as com>

    <#-- Initialize common variables -->
    <@com.initializeMainVariables/>

    <#if _subject.submissionType?has_content && _subject.submissionType?matches("EU_PPP_.*")>
        <@com.initiRelevanceForPPP relevance/>
    </#if>

    <#-- Root entity -->    
    <#assign rootEntity = entity.root>

    <#-- Define the main macro and the arguments to be used to get output using the traversal approach -->
    <#assign mainMacroName = "findStudies"/>
    <#assign mainMacroArgs = ['docElementNode', 'sectionDoc', 'sectionNode', 'entityDocument', 'level']/>

    <#-- Make global the macroCall as well as each macro used in it -->
    <#global mainMacroCall="<@" + mainMacroName + " " + mainMacroArgs?join(" ") + "/>"/>
    <#global findStudies = findStudies/>

    <#-- Print CSV header row -->
    <@printCsvHeadersRow />

    <#-- variables to be used in the code -->
    <#assign selectedDocTypes = ["ENDPOINT_STUDY_RECORD"]/>
    <#assign resultsPathList = ["ResultsAndDiscussion.EffectLevels", "ResultsAndDiscussion.Partcoeff", "ResultsAndDiscussion.WaterSolubility"]/>

    <#-- Do the traverse -->
    <@utils.traversal rootEntity/>

    <#----------------------------------------------->
    <#-- Macro and function definitions start here -->
    <#----------------------------------------------->
    <#-- define header section -->
    <#macro printCsvHeadersRow>
"Entity Type","Entity Name","Section Name","Section Number","waiver","Document Name","Document UUID","Author","Title","Test organisms","Strain / cell type","Route of application / dose method","Exposure duration","Metabolic activation","Metabolic activation system","Key Results","Additional Results","Dossier Name","Dossier Creation Date","IUCLID Version"
    </#macro>

    <#-- call findStudies and determine the elements to search through -->
    <#macro findStudies docElementNode sectionDoc sectionNode entityDocument level>

        <#-- In order to be able to skip unnäccessäärry (¯\_(ツ)_/¯) traversals, we only look at document level and check if we -->
        <#-- have any content (so we know it makes sense to go through the node) -->
        <#if level==0 && selectedDocTypes?seq_contains(sectionDoc.documentType) && docElementNode?has_content>

            <#-- retrieve all literature references for the current node (function defined below) -->
            <#local litRefs = getLiteratureReferences(sectionDoc)/>

            <#-- if there are any literature references -->
            <#if litRefs?has_content>
                <#list litRefs as item> 
                    <@tableOutput item entityDocument sectionNode sectionDoc docElementNode/>
                </#list>
            <#else>
                <#local item=""/>
                <@tableOutput item entityDocument sectionNode sectionDoc docElementNode />
            </#if>
        </#if>
    </#macro>

    <#-- a function to properly create entries as needed: one, if there is one reference, multiple, if there are more than one -->
    <#function getLiteratureReferences docElementNode>
        <#local listOfLiteratureReferences = []/>

            <#-- IS THIS A DUPLICATE to the condition, where to function is called from? curr. Line 47 -->
            <#if docElementNode?has_content> 

                <#-- if it is a node with a single entry -->
                 <#if docElementNode?node_type == "document_reference">
                        <#local listOfLiteratureReferences = checkAndAddLiteraturReference(docElementNode, listOfLiteratureReferences)/>
                 </#if>

                <#-- if it is a node with multiple entries (denote the "s" in the document_reference*s* !) -->
                <#if docElementNode?node_type == "document_references">
                  <#list docElementNode as item>
                    <#local listOfLiteratureReferences = checkAndAddLiteraturReference(item, listOfLiteratureReferences)/>
                  </#list>
                </#if>

                <#if docElementNode?children?has_content>
                    <#list docElementNode?children as child>
                        <#if child?has_content>
                            <#local listOfLiteratureReferences = listOfLiteratureReferences + getLiteratureReferences(child)/>
                        </#if>
                    </#list>
                </#if>

            </#if>
        <#return listOfLiteratureReferences>
    </#function>

    <#function checkAndAddLiteraturReference key listOfLiteratureReferences>

        <#local doc = iuclid.getDocumentForKey(key) />
        <#if doc?has_content>
            <#if doc.documentType == "LITERATURE">

                <#return listOfLiteratureReferences + [doc]>

            </#if>
        </#if>
        <#return listOfLiteratureReferences/>

    </#function>

    <#macro tableOutput litRef entityDocument sectionNode sectionDoc docElementNode >
        
        
            <#local substanceName>${docElementNode?node_type}
                ${sectionDoc?node_type} ${entityDocument?node_type}</#local>
       

         <#-- use compress for squeezing all together and remove empty lines for csv output -->   
         <#if litRef?has_content>
             <#local refTitle><@utils.csvValue litRef.GeneralInfo.Name /></#local>
             <#local refAuthor><@utils.csvValue litRef.GeneralInfo.Name /></#local>

        <#else>
            <#local refTitle=""/>
            <#local refAuthor=""/>


        </#if>

             <#if sectionDoc.AdministrativeData.DataWaiving?has_content>
                 <#-- retrieve the value from the waiver picklist (.value from common modules takes care of data types) -->
                 <#local waiver><@com.value sectionDoc.AdministrativeData.DataWaiving/></#local>   
            <#else>
                <#local waiver=""/>
             </#if>

            <#local dossierName><@utils.csvValue _dossierHeader.name/></#local>
            <#local dossierCreationDate><@utils.csvValue _dossierHeader.creationDate/></#local>
                     
            <#local docUrl=utils.getDocumentUrl(sectionDoc)/>
                     
            <#local species>
                <#compress>
                <#if sectionDoc.hasElement("MaterialsAndMethods.TestAnimals.Species")><#-- path for most Tox ESRs -->
                    <@com.value sectionDoc.MaterialsAndMethods.TestAnimals.Species/>
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.TestOrganisms.TestOrganismsSpecies")><#-- path for most Ecotox ESRs -->
                    <@com.value sectionDoc.MaterialsAndMethods.TestOrganisms.TestOrganismsSpecies/>
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.InVivoTestSystem.TestAnimals.Species")><#-- path for skin sensitisation -->
                    <@com.value sectionDoc.MaterialsAndMethods.InVivoTestSystem.TestAnimals.Species/>
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.Method.SpeciesStrain") && sectionDoc.MaterialsAndMethods.Method.SpeciesStrain?node_type=="repeatable">
                    <#-- example to check the node type: node type: ${sectionDoc.MaterialsAndMethods.Method.SpeciesStrain?node_type} -->
                    <#compress>
                    <#list sectionDoc.MaterialsAndMethods.Method.SpeciesStrain as speciesRow>
                        <@com.value valuePath=speciesRow.SpeciesStrain printDescription=false/><#if speciesRow?has_next> / </#if>
                    </#list>
                    </#compress>
                <#else>
                    field not available
                </#if>
                </#compress>
            
            </#local>
                    
            <#local strainOrCell>
                <#if sectionDoc.hasElement("MaterialsAndMethods.TestAnimals.Strain")>
                    <@com.value valuePath=sectionDoc.MaterialsAndMethods.TestAnimals.Strain printDescription=false/>  
                <#else>
                    field not available
                </#if>
            </#local>
                    
            <#local routeApplicationDoseMethod><#compress>
                <#if sectionDoc.hasElement("MaterialsAndMethods.Studydesign.TestType")>
                     <@com.value sectionDoc.MaterialsAndMethods.Studydesign.TestType/>
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.TestMaterials.DoseMethod")>
                     <@com.value sectionDoc.MaterialsAndMethods.TestMaterials.DoseMethod/>
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.AdministrationExposure.RouteOfAdministration")>
                     <@com.value sectionDoc.MaterialsAndMethods.AdministrationExposure.RouteOfAdministration/>
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.TestSystem.TypeOfCoverage")>
                     <@com.value sectionDoc.MaterialsAndMethods.TestSystem.TypeOfCoverage/>
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.AdministrationExposure.TypeOfCoverage")>
                      <@com.value sectionDoc.MaterialsAndMethods.AdministrationExposure.TypeOfCoverage/>
                <#else>
                     field not available
                </#if>
            </#compress></#local>
            
                    
            <#local exposureDuration><#compress>
                <#if sectionDoc.hasElement("MaterialsAndMethods.StudyDesign.TotalExposureDuration")>
                    <@com.value sectionDoc.MaterialsAndMethods.StudyDesign.TotalExposureDuration/>
                
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.AdministrationExposure.DurationAndFrequencyOfTreatmentExposure")>
                    <@com.value sectionDoc.MaterialsAndMethods.AdministrationExposure.DurationAndFrequencyOfTreatmentExposure/>
                
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.AdministrationExposure.DurationOfExposure")>
                    <@com.value sectionDoc.MaterialsAndMethods.AdministrationExposure.DurationOfExposure/>
                    
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.TestSystem.DurationOfTreatmentExposure")>
                    <@com.value sectionDoc.MaterialsAndMethods.TestSystem.DurationOfTreatmentExposure/>
                    
                <#elseif sectionDoc.hasElement("MaterialsAndMethods.AdministrationExposure.DurationOfTreatmentExposure")>
                    <@com.value sectionDoc.MaterialsAndMethods.AdministrationExposure.DurationOfTreatmentExposure/>
                         
                </#if>
             </#compress></#local>
                    
          <#local metAct><#compress>
             <#if sectionDoc.hasElement("MaterialsAndMethods.Method.MetabolicActivation")>
                 <@com.value sectionDoc.MaterialsAndMethods.Method.MetabolicActivation/>
             </#if>
           </#compress></#local>
         
         <#local metActSystem><#compress>
             <#if sectionDoc.hasElement("MaterialsAndMethods.Method.MetabolicActivationSystem")>
                 <@com.value sectionDoc.MaterialsAndMethods.Method.MetabolicActivationSystem/>
             </#if>
         </#compress></#local>
             
            <#-- Study results: Effect levels -->
            <#local endpointStudyRecordKeyResultsList = []/>
            <#local endpointStudyRecordAddResultsList = []/>
                
            
            <#list resultsPathList as path>
                <#if sectionDoc.hasElement(path)>
                    <#local fullPath = 'sectionDoc.' + path/>
                    <#local fullPath = fullPath?eval/>
                    <#if fullPath?node_type == "repeatable">
                        
                        <#list fullPath as resultRow>
                            
                        
                                <#local resultsList = []/>
                                <#list resultRow?children as child>
                                    <@iuclid.label for=child var="descript"/>

                                    <#if child?has_content && !(descript?contains("Remark") || descript=="Key result")>
                                       
                                        <#local item>${descript}: <@compress single_line=true><@com.value child/></@compress></#local>
                                        <#local resultsList = resultsList + [item]/>
                                      </#if>
                                </#list>
                                <#local result = resultsList?join("; ")/>  
                                    
                                <#if resultRow.hasElement("KeyResult") && resultRow.KeyResult>
                                    <#local endpointStudyRecordKeyResultsList = endpointStudyRecordKeyResultsList + [result] />
                                <#else>
                                    <#local endpointStudyRecordAddResultsList = endpointStudyRecordAddResultsList + [result] />
                                </#if>
                            
                        </#list>
                   </#if>
                </#if>
            </#list>
           
                
            <#local endpointStudyRecordKeyResults = escapeText(endpointStudyRecordKeyResultsList?join(" / "))/>
            <#local endpointStudyRecordAdditionalResults = escapeText(endpointStudyRecordAddResultsList?join(" / "))/>

   
                   
         <#compress>
            <@utils.csvValue entityDocument.documentType/>,<@utils.csvValue entityDocument.name/>,<@utils.csvValue sectionNode.title/>,<@utils.csvValue sectionNode.number/>,<@utils.csvValue substanceName/>,${waiver},<@utils.csvValue sectionDoc.name/>,"=HYPERLINK(""${docUrl}"",""${sectionDoc.documentKey.uuid}"")",${refAuthor},${refTitle},<@utils.csvValue species />,<@utils.csvValue strainOrCell/>,<@utils.csvValue routeApplicationDoseMethod/>,<@utils.csvValue exposureDuration/>,<@utils.csvValue metAct/>,<@utils.csvValue metActSystem/>,<@utils.csvValue endpointStudyRecordKeyResults/>,<@utils.csvValue endpointStudyRecordAdditionalResults/>,${dossierName},${dossierCreationDate},${iuclid6Version!}
        </#compress>
        

    </#macro>

</#compress>

<#function escapeText text>
    <#if text?has_content>
        <#return text?replace("&gt;", ">")?replace("&lt;", "<")/>
    <#else>
        <#return text/>
    </#if>
    
</#function>
<#-- structure -->
    
<#--     ENTITY DOCUMENT -->
<#--        --- section node 1 -->
<#-- 		--- section node 1.1 -->
<#--             Section document 1 -->
<#--                 Lit ref 1 -->
<#--                 Lit ref 2 -->
<#--             Section document 2 -->
<#--                 Lit ref 3 -->
<#--         --- section node 2 -->