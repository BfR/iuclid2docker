# Original/Maintenance
This is (thus far) maintained by BfR and mirrored here for completeness.
See also https://gitlab.opencode.de/BfR/iuclid2docker

# Building an IUCLID System with Multiple Docker Containers

## Motivation
We want to have a development enviroment for report-generator-templates to use with ECHA's IUCLID.  
As the work with templates is quite click-intensive –especially during template-development– but can be automated via API, a docker swarm was created.  
For this, the IUCLID-System and the report-template editor as Jupyter-Notebooks was split into several containers.  
We took inspiration from the [ECHA IUCLID Report Generator Workshop](https://github.com/andresfib/reporting-ws).  

Another advantage of having database and IUCLID-application in separate containers is that it allows for easier maintenance.



## The DB Backend

As suggested by the IUCLID server manual, the back-end DB can be build on PostgreSQL.
The idea is to create a PostgreSQL docker instance which will then house the data of IUCLID.
This will ensure the persistance of the data put into IULCID and the main version of IUCLID could be easily updated without the need for the backup of the whole data base prior version upgrade.

### Building the IUCLID6 Container Stack

> Prerequisites: Before any of the following steps can be executed, please make shure you have [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) installed.  
> There is a nice install guide for Docker Compose available for Ubuntu Systems [here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-22-04).

The main intention for this project is to lower the inhibition threshold as low as possible to get you started with IUCLID6.
Therefore, the whole system is containerised so that it is possible to get rid of everything without anything left on the system.

As depicted in the graph below, there are several container hosting different services and being connected to each other.

```mermaid
graph TD;
  subgraph Host System
    E[Webtools Container];
    F[Jupyter Lab Editor Container];
    C[Adminer Container];
    A[IUCLID Container];
    B[PostgreSQL Container];

    D(Store Data on host);


    C-->|inspects| B;
    A==>|stores data| B;

    B -. default but optional .-> D;
    E -->|links/REST API calls| A;
    E -->|links| C;
    E -->|links| F;

    F -->|edit files e.g. ftl| E;
    B-->C;
  end

  classDef orange fill:#f96,stroke:#333,stroke-width:1px;
  class A,B orange;
```

### Building the PostgreSQL Container

Creation of a docker container using docker compose:

Content of the [`iuclid.yml`](iuclid.yml) file can be adjusted individually if needed.

In order to achieve the afore mentioned goal, docker compose seems to be the best solution.

### Creation of a docker container using docker compose:

The full content of the compose recipe can be seen in the file [`iuclid.yml`](iuclid.yml).

For editing some parameters according to ones personal flavour, the file [`iuclid.env`](iuclid.env) could be adjusted.

### Useful `docker compose` Commands

In order to work properly with `docker-compose`, one has to know some simple commands.

| Syntax      | Description |
| ----------- | ----------- |
| `docker compose --env-file iuclid.env -f iuclid.yml up`      | Starts the services which are described in the designated file; indicated with the `-f` flag.       |
| `docker compose --env-file iuclid.env -f iuclid.yml up -d` | Adding the `-d` flag, will run the services as a daemon (in the background with no CLI output). |
| `docker compose --env-file iuclid.env -f iuclid.yml down` | This will stop the daemon, if started with the daemon flag. |
| `docker compose --env-file iuclid.env -f iuclid.yml ps` | If the service was initiated with the daemon flag, with this command, one can check for all active daemons that are currently running; additionally the `yml` file it has been started with has to be specified; it would also be possible to call `docker container ls` to list all running containers. |
| `docker compose --env-file iuclid.env -f iuclid.yml stop` | This will stop all currently running services that have been started with the daemon flag. |
| `docker compose --env-file iuclid.env -f iuclid.yml down --rmi all` | Wipes all container that have been created based on `iuclid.yml`. This might be usefull for a clean fresh start. |
| `docker compose --env-file iuclid.env -f iuclid.yml up --build --remove-orphans` | This command forces the rebuild of the containers without reloading the images they are based upon. |


After the command
```sh
docker compose --env-file iuclid.env -f iuclid.yml up
```
has been executed, all
services should be up and running.  

There is an even shorter way to start the Docker swarm, by just executing the provided scriptfile:

```sh
./starting_script.sh
```

After running the script, multiple container will be started which interact with each other.
The way they interact is depicted (kind of) in the graph above.

In order to make things easier - and inspired by the [ECHA IUCLID Report Generator Workshop](https://github.com/andresfib/reporting-ws) setup, a landing page is used to navigate to and access the different container services.
Therefore, one has to open the URL [http://localhost:66](http://localhost:66) in a browser tab.
From this page on, the services are easy to reach.  

### Stopping all services and container

Stopping all services is also very easy by running the script file `stop_script.sh` by typing:

```sh
./stop_script.sh
```


### Going Further

#### Adminer Container

In order to access the adminer service, one has to go to the website [http://localhost:1337/](http://localhost:1337/) and enter the following credentials to login:

| Name | Value |
|---|---|
| **System**    | `PostgreSQL`            |
| **Server**    | `iuclid6-db`            |
| **Username**  | `IUCLID6`               |
| **Password**  | `${POSTGRES_PASSWORD}`  |
| **Database**  | `IUCLID6`               |

In this particular test scenario, the `POSTGRES_PASSWORD` will be `test`.

#### First Functionallity Check

After logging in to the Adminer Frontend, the first one should see is a bunch of tables in the data base.
If this is not the case, there is an issue with the connection between the IUCLID6 container and the PostgreSQL container.
During the development I experienced this flaw especially when trying to store the PostgreSQL data persistently on the host system.
As far as I was able to encircle the issue, I realised that the reason might be the chosen access rights for the folder the data shall be permanently stored in, which in this example case is `postgresVolume`.
As the folder was created by the user and docker is run by `root`, `root` might not be able to write to the folder and therefore no tables will be created which are necessary for IUCLID to run smoothly.  
The solution - in some cases so far - is to give access rights for the `postgresVolume` to everyone by using the command:

```sh
sudo chmod 777 postgresVolume
```
> ***NOTE:*** This might cause security issues and has to be considered throroughly.


### IUCLID6 Web Frontend

Going to website [http://localhost:6600/iuclid6-web](http://localhost:6600/iuclid6-web) will grant access to the IUCLID6 webfrontend.  
The login credentials are –in this use case– the standard credentials.
They have not been altered.

| **User** | **Password** |
| --- | --- |
| `SuperUser` | `root` |


> NOTE: Sometimes it can take up to five minutes to initiate the IUCLID instance. So please be patient. :)

### Payara 5 Admin Console

Before it is possible to access the admin web interface, some fixes need to be made, which cannot be executed during the creation of the IUCLID6 container ([suggestions welcome](https://gitlab.com/iuclid/IUCLID2Docker/-/issues/new)).  
In order to activate the secure admin interface, the following commands need to be executed on the payara instance:

```sh
docker compose --env-file iuclid.env -f iuclid.yml exec iuclid6 ./payara5/bin/asadmin --host localhost enable-secure-admin
```

The CLI is asking for a login (`admin`) and a password (`admin123`) (both defined in the [`Dockerfile`](iuclid6-dir/Dockerfile))

In order to activate the changes, the service has to be restarted:

```sh
docker compose --env-file iuclid.env -f iuclid.yml exec iuclid6 ./payara5/bin/asadmin restart-domain
```

After a few moments, the service will be available under [http://localhost:6611](http://localhost:6611) with the same credentials as mentioned above.

> ***NOTE***: For some reason - I do not fully understand - the activation of the secure admin feature of the Payara server seems to have a negative effect on the availability of the IUCLID6 server instance. So be advised: **USE WITH CAUTION** If the setup breaks, one can still rebuild the stack `¯\_(ツ)_/¯`

## Preparing the workflow

Before one can use the dev env as intended, some adjustments have to be made.
Within this section, the individual steps will be described as neat as possible.

1. Create a *dummy* report which will serve as a placeholder for the upcoming reports to cerate.
Therefore, navigate to your IUCLID instance and click on the ***Dashboard*** icon and click on the ***Manage Reports*** item.
![Create a new *dummy* report.](/screenshots/001_Dashboard_Manage_Reports.png)

2. Click on the ***+ New Report*** button to add a new report.
![](/screenshots/002_New_Report.png)

3. Now, the system is asking for a set of information that are mandatory to create your first report template.
Despite the most obvious ones like name and description, one is asked to specify the output file type. 
As ECHA has kind of announced that the switch from DocBook to HTML for report generation is currently prepared, in this section spotlight will be put on the `*.csv` output.
So this is the tickbox to go for. 
![](/screenshots/003_New_Report_settings.png)

4. The next section in the row is the ***Templates*** section.
In here, there are some more tricky adjustments needed.  
First: navigate to the IPython-based editor (if you use the default settings [THIS Link](http://localhost:8888/lab/tree/reporting-dev/extended_lit_ref_new_csv) should bring you there).
Second: Create a file that will be your template (ideally you will call it the same as defined in the ***Name*** box in the above ***General Information*** section)  
![](/screenshots/004_new_template_file.png)
![](/screenshots/005_new_template_file_2.png)  
After this has been done, the just created file has to be uploaded as the ***Main Template***.
If you have used the link provided above and haven't changed anything to the default settings, than, you can find the just created template dummy file in folder:
```bash
iuclid2docker/nginx/reporting-dev/extended_lit_ref_new_csv
```
Additionally, there are some additional templates needed to compile reports the ECHA way.
Those additional files do contain some helpful snippets and macros that make developing report generator templates easier.
The files can be found in the same folder where the dummy template file is located in.
After uploading the files, the ***Templates*** section should look something like this:
![](/screenshots/006_templates_section.png)

5. The ***Stylesheets & Working Context*** definition section will ask to define a Working Context in which the report template will be available.
While we stick to the default stylesheet, in this guide all options for working context will be selected. 
This can then be adjusted to individual needs afterwards.
With clicking on the save button the template will be created and made available to the system.
![](/screenshots/007_Stylesheet_Working_Context.png)

6. **IMPORTANT:** In order to make the automated upload/refresh script work, one needs to know the internal ID of the report.
This can be figured out by clicking on the just created template and examining the URL.
The digits at the very end of the URL are the internal template ID.
Which in this case, is `1`.
![](/screenshots/008_template_id.png)

7. The internal template ID has to be adjusted in the [`extended_lit_ref_new_csv.env`](http://localhost:8888/lab/tree/reporting-dev/env/extended_lit_ref_new_csv.env) file, line 45
```bash
export RF_REPORT_ID=1
```
There is also the need to define the UUID of the substance/dossier you want to develop with, in order to automatically generate the report with just a click on the link at the landing page.
So there is also the need to adjust this in the [`extended_lit_ref_new_csv.env`](http://localhost:8888/lab/tree/reporting-dev/env/extended_lit_ref_new_csv.env) file in line 50:
```bash
export RF_DOSSIERS=(<UUID_of_the_test_dossier>)
```
Additionally, in line 69 and 72, the name of the template file has to be adjusted as well
```bash
export RF_MAIN_FTL=docker_report_generator_env_template
export RF_REPORT_NAME="docker_report_generator_env_template"
```

> Example Data: Within this repo, there is also some [sample data](/nginx/reporting-dev/data/) available to let you start right away (as provided for the IUCLID6 Report Generator Workshop in Nov 2022).
Read those into your local IUCLID6 instance and identify the given UUID, so that it can be adjusted in the `*.env` file.

After the settings have been made, one can use the editor -- based on Jupyter Notebooks -- to edit the template `*.ftl`.

![](/screenshots/009_editing_template.png)

If the `*.ftl` has been edited, one could save the work and initiate the update/refresh to the IUCLID6 instance.
Therefore one has to navigate to the [landing page](localhost:66) and click on the link at the bottom in the tools section:

![](/screenshots/010_trigger_update.png)

In some cases or even for the first time the script has been initated, doing a refresh of the popping page might be necessary to properly trigger the underlying script.
Afterwards, one can see the changes and the freshly generated output file.

![](/screenshots/011_created_report.png)

### Current Findings

- It seems to be rather easy to jump to a newer version of IUCLID6.
All settings and data are stored in the PostgreSQL data base - this includes templates and dossiers.
If  a new version of IUCLID6 is available, one can change the version in the [`iuclid.yml`](/iuclid.yml) file and start the building process using the [`starting_script.sh`](./starting_script.sh).

## TO DO's

- [x] include some arguments for the docker compose process so that the options for the iuclid binary, the password and login values for payara, IUCLID6 and the PostgreSQL don't have to be edited in the [`Dockerfile`](iuclid6-dir/Dockerfile) or the [`iuclid.yml`](iuclid.yml) file (maybe by using some kind of `.env` file that can be added to the process?)
- [x] adding a Jupyter Lab/nginx instance for easier report generator template creation (like it was done at the report generator workshop)
- [ ] writing a function to capture the server output in case the submitted `.ftl` causes an error during report generation (json feedback)
- [ ] writing a more descriptive how-to for the initial setup of the system
- [ ] *... open for discussion ...*
