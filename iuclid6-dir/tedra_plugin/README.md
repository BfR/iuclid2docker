For integrating OECD QSAR Toolbox compatibility in the IUCLID instance th [TEDRA plugin](https://qsartoolbox.org/download/#tedra) has to be put in this folder.  
Secondly, the command for putting it in the correct position in the IUCLID file structure during the build procedure, the corresponding line of code in the [Dockerfile](../Dockerfile) (currently: line 39) needs to be uncommented.

> Currently, I haven't checked compatibility with latest IUCLID versions so any feedback is highly appreciated.
