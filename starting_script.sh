docker compose --env-file iuclid.env -f iuclid.yml up --build --remove-orphans -d
docker compose --env-file iuclid.env -f iuclid.yml exec iuclid6-webtools service supervisor start

echo "Wait 10 seconds to let things settle a little bit ..."
sleep 10

echo "Adminer container:"
docker container inspect adminer | jq '.[] | .State.Status'
echo "IUCLID6-DB container:"
docker container inspect iuclid6-db | jq '.[] | .State.Status'
echo "IUCLID6 container:"
docker container inspect iuclid6 | jq '.[] | .State.Status'
echo "IULCID6-Webtools container:"
docker container inspect iuclid6-webtools | jq '.[] | .State.Status'
echo "iuclid6-template-editor container:"
docker container inspect iuclid6-template-editor | jq '.[] | .State.Status'

echo "If all is up and running \(^o^)/ - visit http://localhost:66 to start developing your templates."
