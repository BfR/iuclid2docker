This folder is just a fork of ECHAs repos for automated report generation using REST API calls with some minor tweaks to them in order to make it functional in the container swarm environmen.

The original source can be found [here](https://github.com/echa-ecm/report_generator_scripts).

Lastly - as mentioned in this repos README - the idea of creating a landing page for the different services is based on ECHAs Report Generator Workshop and its repository can be found [here](https://github.com/andresfib/reporting-ws).
